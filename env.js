const path = require('path');
const dotenv = require('dotenv');

const { error } = dotenv.config({ debug: process.env.DEBUG });

if (error) {
  if (error.code === 'ENOENT') {
    const defaultPath = path.resolve(process.cwd(), '.env');
    throw new Error(
      `No .env file was found. Did you forget to add one? Default path is "${defaultPath}"`
    );
  }
  throw error;
}
