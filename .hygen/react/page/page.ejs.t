---
to: src/pages/<%= name %>.js
---
import React from 'react';

import Page from '~layout/Page';
import SEO from '~layout/SEO';

import Container from '~components/Container';

const <%= Name %>Page = () => (
  <Page>
    <SEO />
    <Container>
      <h1>Page <%= Name %></h1>
    </Container>
  </Page>
);

export default <%= Name %>Page;
