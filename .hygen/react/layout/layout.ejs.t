---
to: src/layout/<%= Name %>.js
---
import React from 'react';
import PropTypes from 'prop-types';

const <%= Name %> = () => {};

<%= Name %>.propTypes = {};

export default <%= Name %>;
