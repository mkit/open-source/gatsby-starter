---
to: src/containers/<%= Name %>.js
---
import React from 'react';
import PropTypes from 'prop-types';

class <%= Name %> extends React.Component {
  render() {
    return null;
  }
}

<%= Name %>.propTypes = {};

export default <%= Name %>;
