require('./env');
const path = require('path');

module.exports = {
  siteMetadata: {
    siteUrl: process.env.SITE_URL, // required by gatsby-plugin-robots-txt
    siteTitle: process.env.SITE_TITLE,
    siteTitleShort: process.env.SITE_TITLE_SHORT,
    siteDescription: process.env.SITE_DESCRIPTION,
    siteKeywords: process.env.SITE_KEYWORDS,
    siteThemeColor: process.env.SITE_THEME_COLOR,
    siteSocialImageUrl: process.env.SITE_SOCIAL_IMAGE_URL,
    twitterAccountId: process.env.TWITTER_ACCOUNT_ID,
    facebookAppId: process.env.FACEBOOK_APP_ID
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-material-ui',
      options: {}
    },
    {
      resolve: 'gatsby-plugin-prefetch-google-fonts',
      options: {
        fonts: [
          {
            family: 'Roboto',
            variants: ['300', '400', '500', '700']
          }
        ]
      }
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: process.env.SITE_THEME_COLOR
      }
    },
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_ID,
        head: true
      }
    },
    {
      resolve: `gatsby-plugin-facebook-pixel`,
      options: {
        pixelId: process.env.FACEBOOK_PIXEL_ID
      }
    },
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint: process.env.MAILCHIMP_ENDPOINT
      }
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: process.env.SITE_TITLE,
        short_name: process.env.SITE_TITLE_SHORT,
        start_url: '/',
        theme_color: process.env.SITE_THEME_COLOR,
        background_color: process.env.SITE_BACKGROUND_COLOR,
        display: 'minimal-ui',
        icon: path.resolve(__dirname, process.env.SITE_LOGO_IMAGE_PATH)
      }
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        '~src': path.join(__dirname, 'src'),
        '~pages': path.join(__dirname, 'src/pages'),
        '~layout': path.join(__dirname, 'src/layout'),
        '~containers': path.join(__dirname, 'src/containers'),
        '~components': path.join(__dirname, 'src/components')
      }
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-robots-txt',
    'gatsby-plugin-sitemap',
    'gatsby-plugin-offline',
    'gatsby-plugin-webpack-size'
  ]
};
