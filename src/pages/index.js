import React from 'react';

import Page from '~layout/Page';
import SEO from '~layout/SEO';

import Container from '~components/Container';
import { LinkAsText } from '~components/Link';

const IndexPage = () => (
  <Page>
    <SEO pageTitle="Welcome" />
    <Container>
      <h1>Welcome!</h1>
      <h3>Gatsby - Material UI Starter</h3>
      <p>Essential components and layout can be found under:</p>
      <ul>
        <li>
          <code>src/components</code>
        </li>
        <li>
          <code>src/layout</code>
        </li>
      </ul>
      <p>MK IT&apos;s opinionated theme can be found under:</p>
      <ul>
        <li>
          <code>src/theme</code>
        </li>
      </ul>
      <p>Available pages:</p>
      <ul>
        <li>
          <LinkAsText to="/404">Custom 404 page</LinkAsText>
        </li>
      </ul>
    </Container>
  </Page>
);

export default IndexPage;
