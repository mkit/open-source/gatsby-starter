import React from 'react';

import Page from '~layout/Page';
import SEO from '~layout/SEO';

import Container from '~components/Container';
import { LinkAsText } from '~components/Link';

const NotFoundPage = () => (
  <Page>
    <SEO pageTitle="404" />
    <Container>
      <h1>404</h1>
      <h3>Page not found</h3>
      <LinkAsText to="/">Back to Home</LinkAsText>
    </Container>
  </Page>
);

export default NotFoundPage;
