import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import responsiveFontSizes from '@material-ui/core/styles/responsiveFontSizes';
import teal from '@material-ui/core/colors/teal';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: pink,
    error: red,
    background: {
      default: '#fff'
    }
  }
});

const themeWithResponsiveFonts = responsiveFontSizes(theme);

export default themeWithResponsiveFonts;
