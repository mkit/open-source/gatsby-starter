import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { StaticQuery, graphql } from 'gatsby';

const query = graphql`
  query {
    site {
      siteMetadata {
        siteUrl
        siteTitle
        siteDescription
        siteKeywords
        siteThemeColor
        siteSocialImageUrl
        twitterAccountId
        facebookAppId
      }
    }
  }
`;

const SEO = ({
  pageTitle,
  pageDescription,
  pageKeywords,
  pageImage,
  pageCanonicalUrl,
  site
}) => {
  const {
    siteUrl,
    siteTitle,
    siteDescription,
    siteKeywords,
    siteThemeColor,
    siteSocialImageUrl,
    twitterAccountId,
    facebookAppId
  } = site;
  const pageTitleFull = pageTitle ? `${pageTitle} | ${siteTitle}` : siteTitle;
  const pageDescriptionFull = pageDescription || siteDescription;
  const pageKeywordsFull = pageKeywords || siteKeywords;
  const pageImageFull = pageImage || `${siteUrl}${siteSocialImageUrl}`;

  return (
    <Helmet>
      <html lang="en" />
      <meta content="IE=edge" httpEquiv="X-UA-Compatible" />

      {/* Title */}
      <meta content={siteTitle} name="apple-mobile-web-app-title" />
      <meta content={pageTitleFull} property="og:title" />
      <meta content={pageTitleFull} name="twitter:title" />
      <title>{pageTitleFull}</title>
      <meta content={pageKeywordsFull} name="keywords" />
      <meta content={pageDescriptionFull} name="description" />
      <meta content={pageDescriptionFull} property="og:description" />
      <meta content={pageDescriptionFull} name="twitter:description" />
      <meta content="yes" name="apple-mobile-web-app-capable" />
      <meta
        content="black-translucent"
        name="apple-mobile-web-app-status-bar-style"
      />
      <meta content={siteThemeColor} name="theme-color" />
      <meta content={siteTitle} name="application-name" />

      {/* Title, URL */}
      <meta content="website" property="og:type" />
      <meta content={siteTitle} property="og:site_name" />
      <meta content={facebookAppId} property="fb:app_id" />
      <meta content="summary_large_image" name="twitter:card" />
      <meta content={`@${twitterAccountId}`} name="twitter:site" />
      <meta content={`@${twitterAccountId}`} name="twitter:creator" />
      <meta content={pageTitleFull} name="twitter:text:title" />
      {pageCanonicalUrl && (
        <meta content={pageCanonicalUrl} property="og:url" />
      )}
      {pageCanonicalUrl && (
        <meta content={pageCanonicalUrl} name="twitter:url" />
      )}
      {pageCanonicalUrl && <link rel="canonical" href={pageCanonicalUrl} />}

      {/* Images */}
      <meta content={pageImageFull} property="og:image" />
      <meta content="1024" property="og:image:width" />
      <meta content="512" property="og:image:height" />
      <meta content={pageImageFull} name="twitter:image" />
      <meta content="1024" name="twitter:image:width" />
      <meta content="512" name="twitter:image:height" />

      {/* JSON-LD */}
      <script type="application/ld+json">
        {JSON.stringify({
          '@context': 'http://schema.org',
          '@type': 'Organization',
          name: 'Make IT',
          legalName: 'MK IT Ltd.',
          alternateName: 'Proficient In Code, Curious About People | MK IT',
          foundingDate: '2016',
          url: 'https://mkit.io',
          logo: 'https://mkit.io/images/site_logo.png'
        })}
      </script>
    </Helmet>
  );
};

SEO.propTypes = {
  site: PropTypes.shape({
    siteUrl: PropTypes.string.isRequired,
    siteTitle: PropTypes.string.isRequired,
    siteDescription: PropTypes.string.isRequired,
    siteKeywords: PropTypes.string.isRequired,
    siteThemeColor: PropTypes.string.isRequired,
    siteSocialImageUrl: PropTypes.string.isRequired,
    twitterAccountId: PropTypes.string.isRequired,
    facebookAppId: PropTypes.string.isRequired
  }).isRequired,
  pageTitle: PropTypes.string,
  pageDescription: PropTypes.string,
  pageKeywords: PropTypes.string,
  pageImage: PropTypes.string,
  pageCanonicalUrl: PropTypes.string
};

SEO.defaultProps = {
  pageTitle: null,
  pageDescription: null,
  pageKeywords: null,
  pageImage: null,
  pageCanonicalUrl: null
};

const withStaticQuery = props => (
  <StaticQuery
    query={query}
    render={graphqlData => (
      <SEO site={graphqlData.site.siteMetadata} {...props} />
    )}
  />
);

export default withStaticQuery;
