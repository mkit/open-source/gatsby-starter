import React from 'react';
import PropTypes from 'prop-types';

import SEO from '~layout/SEO';

const Page = ({ children }) => {
  return (
    <React.Fragment>
      <SEO />

      {/* PLACEHOLDER FOR NAVBAR */}

      <main>{children}</main>

      {/* PLACEHOLDER FOR FOOTER */}
    </React.Fragment>
  );
};

Page.propTypes = {
  children: PropTypes.node.isRequired
};

export default Page;
