import React from 'react';
import PropTypes from 'prop-types';
import { Link as GatsbyLink } from 'gatsby';
import MuiLink from '@material-ui/core/Link';

import { isInternalLink, isInternalFile } from './utils';

const LinkAsText = ({ children, to, ...rest }) => {
  if (isInternalLink(to)) {
    if (isInternalFile(to)) {
      return (
        <MuiLink variant="body1" href={to} {...rest}>
          {children}
        </MuiLink>
      );
    }

    return (
      <MuiLink variant="body1" component={GatsbyLink} to={to} {...rest}>
        {children}
      </MuiLink>
    );
  }
  return (
    <MuiLink variant="body1" href={to} {...rest}>
      {children}
    </MuiLink>
  );
};

LinkAsText.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired
};

export default LinkAsText;
