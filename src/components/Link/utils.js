export const isInternalLink = to => /^\/(?!\/)/.test(to);

export const isInternalFile = to => /\.[0-9a-z]+$/i.test(to);
