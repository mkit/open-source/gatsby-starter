import React from 'react';
import PropTypes from 'prop-types';
import { Link as GatsbyLink } from 'gatsby';
import MuiButton from '@material-ui/core/Button';

import { isInternalLink, isInternalFile } from './utils';

const LinkAsButton = ({ children, to, ...rest }) => {
  if (isInternalLink(to)) {
    if (isInternalFile(to)) {
      return (
        <MuiButton href={to} {...rest}>
          {children}
        </MuiButton>
      );
    }

    return (
      <MuiButton component={GatsbyLink} to={to} {...rest}>
        {children}
      </MuiButton>
    );
  }
  return (
    <MuiButton href={to} {...rest}>
      {children}
    </MuiButton>
  );
};

LinkAsButton.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired
};

export default LinkAsButton;
