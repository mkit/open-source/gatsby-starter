# Roadmap

The roadmap is a living document, and it is likely that priorities will change, but the list below should give some indication of our plans for the next major release, and for the future.

## Must-have features

- [ ] Use `mkitio@gatsby-theme-password-protect` to restrict access to the app on a global level as prerequisite of going live from Day One.

- [ ] Enable launch on Day One through CI/CD (GitLab pages or Netlify) and app-password-protection.

## Nice-to-have features

## Considerable features
